#!/bin/bash
read -p "Do you want to change the theme ?? (Enter 'change', 'yes', or 'y'): " name
# Verify If the file exists
FILE="$HOME/dotfiles/i3"
CONFIG="$HOME/dotfiles/i3/config"
FOCUS="$HOME/dotfiles/i3/focus"
BEAUTY="$HOME/dotfiles/i3/beauty"


function change_user(){
    # Verify the input of the user
    if [ "$name" == "change" ] || [ "$name" == "yes" ] || [ "$name" == "y" ]; then

        CONFIG="$HOME/dotfiles/i3/config"
        # Verify the theme running actually
        if [ $(cat $CONFIG | sed '197!d' | awk -F ' ' '{print $2}') == "i3status" ]; then
            echo "You where in FOCUS Mode"
            rm -rf $CONFIG
            cp -r $BEAUTY $CONFIG
            echo "Now, you are in BEAUTY Mode"
            # Parse the result of the command i3-msg
            if [ $(i3-msg restart | jq '.[0].success') == true ]; then
                echo "Perfect..."
            else
                echo "Some error restarting i3."
            fi
        else
            echo "You where in BEAUTY Mode"
            pkill polybar
            rm -rf $CONFIG
            cp -r $FOCUS $CONFIG
            echo "Now, you are in FOCUS Mode"
            pkill picom
            if [ $(i3-msg restart | jq '.[0].success') == true ]; then
                echo "Perfect..."
            else
                echo "Some error restarting i3."
            fi
        fi
    else
        echo "OK, we are not gonna change..."
    fi
}

# To Execute the FUNCTION change_user
if [[ "$FILE" ]]; then
    echo "The file exist"
    change_user
else
    echo "The file config don't exist"
    echo "Creating the File................."
    echo "Created file" >> "$HOME/dotfiles/i3/config"
    change_user
fi

