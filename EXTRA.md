## For NeoVim
Install and update multiple times the neovim(Packer)
```bash
:PackerInstall(multiple times)
:PackerUpdate
```

wget https://download.eclipse.org/jdtls/milestones/1.17.0/jdt-language-server-1.17.0-202210271413.tar.gz
tar xvf jdt-language-server-1.17.0-202210271413.tar.gz

Install python-lsp-server, pyright(optional)

- For Python
```bash
sudo pacman -S python-lsp-server
```

- For Java
Start reading this website https://github.com/mfussenegger/nvim-jdtls

## Install GitHub Copilot
```bash
git clone https://github.com/github/copilot.vim.git \
  ~/.config/nvim/pack/github/start/copilot.vim
```

## For LINUX <--
## Add to inverse the mouse click. 

Edit /etc/X11/xorg.conf.d/00-keyboard.conf to set the keyboard distribution.

```bash
Section "InputClass"
        Identifier "system-keyboard"
        Option "XkbLayout" "us"
        Option "XkbModel" "pc105"
        Option "XkbVariant" "dvp"
EndSection
```
For the automatic change of the themes only add a syslink to the file dotfiles/i3/script_theme.sh

Edit /etc/X11/xorg.conf.d/20-touchpad.conf to set the touchpad distribution.

```bash
Section "InputClass"
    Identifier "libinput touchpad catchall"
        Driver "libinput"
        Option "MiddleEmulation" "on"
        Option "Tapping" "on"
        Option "DisableWhileTyping" "on"
        Option "NaturalScrolling" "on"
EndSection
```

### For set the background 
ln -s /home/user/dotfiles/wallpaper/bri.jpg /usr/share/wallpaper/bri.jpg

## To obtain i3 gaps rounded
https://aur.archlinux.org/i3-gaps-rounded-git.git

## The Theme JetBrains Nerd Font
https://aur.archlinux.org/nerd-fonts-jetbrains-mono.git
## For Oh-My-ZSH
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
## For AUTOTILING
git clone https://aur.archlinux.org/autotiling.git && cd autotiling && makepkg -si
## For FZF
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install
## For VIM
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
## For ICON THEME
git clone https://github.com/vinceliuice/Colloid-icon-theme.git
## For the THEME
git clone https://github.com/vinceliuice/WhiteSur-gtk-theme.git
git clone https://github.com/vinceliuice/Orchis-theme.git
cd WhiteSur-gtk-theme && ./install.sh
## For CURSOR THEME
git clone https://github.com/vinceliuice/McMojave-cursors.git
cd MCMojave-cursors && ./install.sh
## For TMUX PLUGINS
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm

## To VIEW MY ACTIVITY
git clone https://aur.archlinux.org/activitywatch-bin.git
