# My (dotfiles)

Sample pictures

![sample1](https://user-images.githubusercontent.com/93055563/179432913-725510ea-2701-4458-a70c-ae46b58a1f07.png)
![sample2](https://user-images.githubusercontent.com/93055563/179432919-7558d18e-e693-4f27-9529-55a475858f1e.png)

These are the list of tools:

- i3-gaps-rounded
- picom-jonaburg
- polybar
- whitesur-gtk-greeter
- Colloid Theme
- McMojave cursors
- alacritty
- oh_my_bash(installation with git mode for keymaps)
- rofi
- feh
- autotiling
- neovim(packer, neotree, treesitter, onedark, jdtls-launcher, etc.)
- vim(vim-plug, fzf, nord, etc.)
- tmux(tpm, tmux-resurrect, tmux-restore)
- picom-jonaburg-fix
- bash-autocomplete
- nerd-fonts-jetbrains-mono
- lxappearance
- pulseaudio
- pavucontrol

