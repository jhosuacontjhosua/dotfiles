if [[ $(whoami) = "root" ]];then

printf "You're root \n"
sudo -i << EOO
# Configure KEYBOARD
# Check if the config exist

if [[ -f /etc/X11/xorg.conf.d/00-keyboard.conf ]]; then

printf " File 00-keyboard already configured \n"

else

sudo cat << EON >> /etc/X11/xorg.conf.d/00-keyboard.conf

Section "InputClass"
    Identifier "system-keyboard"
    Option "XkbLayout" "us"
    Option "XkbModel" "pc105"
    Option "XkbVariant" "dvp"
EndSection
EON

fi

# Configure TOUCHPAD
# Check if the config exists

if [[ -f /etc/X11/xorg.conf.d/20-touchpad.conf ]]; then

printf " File 20-touchpad already configured \n"

else

sudo cat  << EOT >> /etc/X11/xorg.conf.d/20-touchpad.conf
Section "InputClass"
    Identifier "libinput touchpad catchall"
        Driver "libinput"
        Option "MiddleEmulation" "on"
        Option "Tapping" "on"
        Option "DisableWhileTyping" "on"
        Option "NaturalScrolling" "on"
EndSection
EOT

fi

EOO
fi
