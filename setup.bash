LOCATION="/home/$(whoami)/"
mkdir $LOCATION/.config

sudo -i << EOO
# Configure KEYBOARD
# Check if the config exist

if [[ -f /etc/X11/xorg.conf.d/00-keyboard.conf ]]; then

printf " File 00-keyboard already configured \n"

else

sudo cat << EON >> /etc/X11/xorg.conf.d/00-keyboard.conf

Section "InputClass"
    Identifier "system-keyboard"
    Option "XkbLayout" "us"
    Option "XkbModel" "pc105"
    Option "XkbVariant" "dvp"
EndSection
EON

fi

# Configure TOUCHPAD
# Check if the config exists

if [[ -f /etc/X11/xorg.conf.d/20-touchpad.conf ]]; then

printf " File 20-touchpad already configured \n"

else

sudo cat  << EOT >> /etc/X11/xorg.conf.d/20-touchpad.conf
Section "InputClass"
    Identifier "libinput touchpad catchall"
        Driver "libinput"
        Option "MiddleEmulation" "on"
        Option "Tapping" "on"
        Option "DisableWhileTyping" "on"
        Option "NaturalScrolling" "on"
EndSection
EOT

fi

EOO

# FINISH THE INSTALLATION OF FUNDAMENTAL PACKAGES

# Installation of BASE_DEVEL
sudo pacman -S base-devel

# Installation of i3
if pacman -Q | grep i3-gaps-rounded-git;
then
	printf " i3-gaps installed -n"
else
	printf "INSTALLING I3 \n"
    rm -rf $LOCATION/.config/i3
	cd $LOCATION
	git clone https://aur.archlinux.org/i3-gaps-rounded-git.git
	cd i3-gaps-rounded-git
	makepkg -si
	cd $LOCATION
	rm -rf i3-gaps-rounded-git
fi

packages=( "xorg-xbacklight" "alacritty" "i3status" "rofi" "vim" "neovim" "dhcpcd" "firefox" "tmux" "zsh" "feh" "picom" "wget" "jq" "polybar" "lxappearance" "thunar" "pavucontrol" "plymouth" "unzip" "npm")
# then numlock from mkinitcpio-numlock(AUR)

for str in ${packages[@]}; do
	if pacman -Q | grep "$str ";
	then
		printf " The package $str it's installed. \n"
	else
		printf "The package $str it's missing. \n"
		sudo pacman -S $str
	fi
done

additional_packages=( "alsa-utils" "alsa-firmware" "sof-firmware" )

printf "\nDo you want alsa firmware and plugins ? y/n: "
read confirmation_alsa
if [ confirmation_alsa == "0" ];then
    for str_a in ${additional_packages[@]}; do
        if pacman -Q | grep "$str_a ";
        then
            printf " The package $str_a it's installed. \n"
        else
            printf "The package $str_a it's missing. \n"
            sudo pacman -S $str_a
        fi
    done
fi

# CONFIGURATION OF FILES
#
# First Configure JetBrains Font

if pacman -Q | grep nerd-fonts-jetbrains-mono;
then
	rm -rf $LOCATION/nerd-fonts-jetbrains-mono
	printf " THE FONT nerd-fonts-jetbrains-mono already exist\n"
else
	cd $LOCATION
	rm -rf $LOCATION/nerd-fonts-jetbrains-mono
	git clone https://aur.archlinux.org/nerd-fonts-jetbrains-mono.git
	cd $LOCATION/nerd-fonts-jetbrains-mono
	printf "Nerd Fonts JetBrains installing \n"
	makepkg -si
	printf "INSTALLED nerd-font \n"
	cd $LOCATION
fi

# For SDDM
if [[ ! -f /usr/share/sddm/themes/aerial-sddm-theme/theme.conf ]]; then
    sudo pacman -S sddm gst-libav phonon-qt5-gstreamer gst-plugins-good qt5-quickcontrols qt5-graphicaleffects qt5-multimedia
	printf " The theme aerial of SDDM configured. \n"
    sudo ln -s $LOCATION/dotfiles/sddm.conf /etc/sddm.conf
    sudo cp -r $LOCATION/dotfiles/aerial-sddm-theme /usr/share/sddm/themes/.
    sudo systemctl disable lightdm.service
    sudo pacman -Rs sddm
fi

# For I3-gap
if [[ -f $LOCATION/.config/i3/focus ]];then

	printf " The config i3 already exist. \n"

else

	rm -rf $LOCATION/.config/i3
	ln -s $LOCATION/dotfiles/i3 $LOCATION/.config/i3
	printf " I3 config created. \n"
	i3-msg restart

fi

# For Autotiling
if ! command -v autotiling ;
then
	printf "File don't found \n"
	cd $LOCATION
	git clone https://aur.archlinux.org/autotiling.git
	cd $LOCATION/autotiling
	makepkg -si
	rm -rf $LOCATION/autotiling 
	printf " Autotiling installed. \n"
	rm -rf $LOCATION/autotiling
else
	printf " Autotiling is installed \n"
fi

# For FZF
if [ -f $LOCATION/.fzf.zsh ];
then
	printf " Fzf is configured \n"
else
	# Install FZF
	rm -rf $LOCATION/.fzf.bash
	git clone --depth 1 https://github.com/junegunn/fzf.git $LOCATION/.fzf
	cd $LOCATION/.fzf
	./install
	rm -rf install
	printf " Fzf config created. \n"
fi

# For ZSH -- CONFIGURE
if [ -f $LOCATION/.zshrc ];
then
	printf " The config ./zshrc exist. \n"
else

	# Install ZSH
	rm -rf $LOCATION/.zshrc
	rm -rf $LOCATION/.oh-my-zsh
	cd $LOCATION
	chsh -s $(which zsh)

	# Copy .oh-my-zsh
	git clone https://github.com/ohmyzsh/ohmyzsh.git
	mv $LOCATION/ohmyzsh $LOCATION/.oh-my-zsh/

	# Create File of Configuration
	ln -s $LOCATION/dotfiles/.zshrc $LOCATION/.zshrc
	printf "zsh is CREATED. \n"

fi

# For TMUX
if [ -f $LOCATION/.tmux.conf ];
then
	printf " Tmux config exist. \n"
else
	rm -rf $LOCATION/.tmux.conf
	ln -s $LOCATION/dotfiles/tmux.conf $LOCATION/.tmux.conf
	printf " Tmux config created. \n"
fi

# For PICOM
if [ -f $LOCATION/.config/picom/picom.conf ];
then
	printf " Picom config exist. \n"
else
	rm -rf $LOCATION/.config/picom
	ln -s $LOCATION/dotfiles/picom $LOCATION/.config/picom
	printf " Picom config created. \n"
fi

# For ALACRITTY
if [ -f $LOCATION/.config/alacritty/alacritty.toml ];
then
	printf " Alacritty config exist. \n"
else
	rm -rf $LOCATION/.config/alacritty
	ln -s $LOCATION/dotfiles/alacritty $LOCATION/.config/alacritty
	printf " Alacritty config created. \n"
fi

# For ROFI
if [ -f $LOCATION/.config/rofi/config.rasi ];
then
	printf " ROFI config exist. \n"
else
	ln -s $LOCATION/dotfiles/rofi $LOCATION/.config/rofi
	printf " ROFI config created. \n"
fi

# For SCT command to change theme I3
if [ -f /usr/bin/sct ];
then
	printf " SCT exist. \n"
else
	sudo ln -s $LOCATION/.config/i3/script_theme.sh /usr/bin/sct
	printf " SCT config created. \n"
fi

# Background IMAGES
if [ ! -f /usr/share/wallpaper/splash.jpg ];
then
	printf " Link to the wallpaper. \n"
	sudo ln -s $LOCATION/dotfiles/wallpaper /usr/share/wallpaper
else
	printf " Wallpaper exist. \n"
fi

# Config POLYBAR
if [ -f $LOCATION/.config/polybar/config ];
then
	printf " Polybar config exist. \n"
else
	printf " Link to the polybar. \n"
	sudo ln -s $LOCATION/dotfiles/polybar $LOCATION/.config/polybar
fi

# Config VIM
if [ -f $LOCATION/.vimrc ];
then
	printf " Vim config exist. \n"
else
	ln -s $LOCATION/dotfiles/.vimrc $LOCATION/.vimrc
	curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	printf " Link to vimrc. \n"
fi

# Config TPM(tmux)
if [ -f $LOCATION/.tmux/plugins/tpm/tpm ];
then
    	printf " Tmux config exist. \n"
else
    	printf " Configured tmux. \n"
    	git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
fi

#### CONFIGURE THEME

if [ ! -f /usr/share/themes/WhiteSur-Dark/index.theme ];
then
	printf " Configuring THEME \n"
	cd $LOCATION
	git clone https://github.com/vinceliuice/WhiteSur-gtk-theme.git --depth=1
	cd $LOCATION/WhiteSur-gtk-theme
	sudo ./install.sh
	cd $LOCATION && rm -rf WhiteSur-gtk-theme 
else
    printf " Theme configured \n"
fi

#### Config Icons

if [ ! -f $LOCATION/.local/share/icons/WhiteSur-dark/index.theme ];
then

	cd $LOCATION
	git clone https://github.com/vinceliuice/WhiteSur-icon-theme
	cd WhiteSur-icon-theme
	./install.sh
	cd $LOCATION && rm -rf WhiteSur-icon-theme

fi

#### Config Cursor
if [ ! -f $LOCATION/.local/share/icons/McMojave-cursors/index.theme ];
then
	cd $LOCATION
	git clone https://github.com/vinceliuice/McMojave-cursors.git
	cd McMojave-cursors
	./install.sh
	cd $LOCATION && rm -rf McMojave-cursors

fi

if [[ ! $(plymouth-set-default-theme -l | grep 'red_loader') ]]; then

sudo -i << 'EAA'
# Download plymouth-themes
printf "Downloading themes."
cd $LOCATION
git clone https://github.com/adi1090x/plymouth-themes.git $LOCATION/plymouth-themes
cd $LOCATION/plymouth-themes
# Copy themes
printf "Copying themes."
sudo cp -r $LOCATION/plymouth-themes/pack_1/** /usr/share/plymouth/themes/.
sudo cp -r $LOCATION/plymouth-themes/pack_2/** /usr/share/plymouth/themes/.
sudo cp -r $LOCATION/plymouth-themes/pack_3/** /usr/share/plymouth/themes/.
sudo cp -r $LOCATION/plymouth-themes/pack_4/** /usr/share/plymouth/themes/.
# Configure mkinitcpio.conf
#sudo rm -rf /etc/mkinitcpio.conf
#sudo cat << 'EOL' >> /etc/mkinitcpio.conf
#MODULES=(i915)
#BINARIES=()
#FILES=()
#HOOKS=(base udev keyboard keymap plymouth plymouth-encrypt autodetect modconf block filesystems fsck)
#EOL
# Configure GRUB
#sudo awk 'NR==6 {$0="GRUB_CMDLINE_LINUX_DEFAULT=\"loglevel=3 quiet splash vt.global_cursor_default=0\""} { print }' /etc/default/grub >> /etc/default/grub.tmp
#sudo rm -rf /etc/default/grub
#sudo mv /etc/default/grub.tmp /etc/default/grub
#sudo grub-mkconfig -o /boot/grub/grub.cfg
# Reloading
sudo plymouth-set-default-theme -R red_loader
cd $LOCATION
EAA

else
    printf " Plymouth is already configured. \n"
fi


### CONFIGURE VIM
if [ ! -f $LOCATION/.vim/autoload/plug.vim ];
then
    printf "Installing plug.vim \n"
    curl -fLo $LOCATION/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
elif [ ! -f $LOCATION/.vim/plugged/fzf.vim/bin/preview.sh ];
then
    printf "Configuring plugins \n"
    vim -c 'PlugInstall'
else
    printf " VIM already configured \n"
fi

### CONFIGURE NVIM
if [[ ! -f $LOCATION/.config/nvim/init.lua ]]; then
    printf " Configuring NVIM \n"
    ln -s $LOCATION/dotfiles/nvim $LOCATION/.config/nvim
    mkdir $LOCATION/dotfiles/jdtls/
    cd $LOCATION/dotfiles/jdtls/
    wget https://download.eclipse.org/jdtls/milestones/1.39.0/jdt-language-server-1.39.0-202408291433.tar.gz
    tar xvf jdt-language-server-1.39.0-202408291433.tar.gz
    rm -rf $LOCATION/dotfiles/jdtls/jdt-language-server-1.39.0-202408291433.tar.gz
else
    printf " NVIM is already configured \n"
fi

### FINAL PART
if [ ! -f /usr/bin/vi ];
then
    sudo ln -s /usr/bin/vim /usr/bin/vi
fi

xrandr --output Virtual-1 --mode 1360x768

printf "Enter 'y' to logout ENTER and ignore :  \n"
read confirmation
if [ confirmation == "reboot" ];then
	printf " reboot now \n"
    reboot
fi
