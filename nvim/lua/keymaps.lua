local map = vim.api.nvim_set_keymap

local options = { noremap = true }
map('n', '<Space>', '', {})
vim.g.mapleader = ' '
map('n', '<C-t>', '<cmd>Neotree toggle<cr>', options)

-- Telescope
map('n', '<leader>ff',"<cmd>lua require'telescope.builtin'.find_files(require('telescope.themes').get_dropdown({previewer=false,layout_config ={width=0.6,height=0.6}}))<CR>",options)
map('n', '<leader>fg',"<cmd>lua require'telescope.builtin'.live_grep(require('telescope.themes').get_ivy({previewer=false,layout_config ={width=0.6,height=0.6}}))<CR>",options)
map('n', '<leader>fb',"<cmd>lua require'telescope.builtin'.buffers(require('telescope.themes').get_dropdown({previewer=false,layout_config ={width=0.6,height=0.5}}))<CR>",options)
map('n', '<leader>fd',"<cmd>lua require'telescope.builtin'.diagnostics(require('telescope.themes').get_ivy({previewer=false,layout_config ={width=0.6,height=0.4}}))<CR>",options)
map('n', '<leader>fh',"<cmd>lua require'telescope.builtin'.help_tags(require('telescope.themes').get_dropdown({previewer=false,layout_config ={width=0.6,height=0.6}}))<CR>",options)
map('n', '<leader>fr',"<cmd>lua require'telescope.builtin'.lsp_references(require('telescope.themes').get_dropdown({previewer=false,layout_config ={width=0.6,height=0.6}}))<CR>",options)
