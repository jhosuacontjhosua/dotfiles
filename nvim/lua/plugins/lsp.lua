local lspconfig = require('lspconfig')
local opts = { noremap = true, silent = true }
vim.keymap.set('n', '<space>e', vim.diagnostic.open_float, opts)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, opts)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, opts)
vim.keymap.set('n', '<space>q', vim.diagnostic.setloclist, opts)

local on_attach = function(client, bufnr)
    local bufopts = { noremap = true, silent = true, buffer = bufnr }
    vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, bufopts)
    vim.keymap.set('n', 'gd', vim.lsp.buf.definition, bufopts)
    vim.keymap.set('n', 'K', vim.lsp.buf.hover, bufopts)
    vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, bufopts)
    vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, bufopts)
    vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, bufopts)
    vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, bufopts)
    vim.keymap.set('n', '<space>wl', function()
        print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
    end, bufopts)
    vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, bufopts)
    vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, bufopts)
    vim.keymap.set('n', '<space>ca', vim.lsp.buf.code_action, bufopts)
    vim.keymap.set('n', '<space>gr', vim.lsp.buf.references, bufopts)
    vim.keymap.set('n', '<space>f', function() vim.lsp.buf.format { async = true } end, bufopts)
end

-- Border into cmp
local function border(hl_name)
    return {
        { "╭", hl_name },
        { "─", hl_name },
        { "╮", hl_name },
        { "│", hl_name },
        { "╯", hl_name },
        { "─", hl_name },
        { "╰", hl_name },
        { "│", hl_name },
    }
end

local cmp_window = require "cmp.utils.window"

cmp_window.info_ = function(self)
    local info = self:info_()
    info.scrollable = true
    return info
end

local has_words_before = function()
    if vim.api.nvim_buf_get_option(0, "buftype") == "prompt" then return false end
    local line, col = unpack(vim.api.nvim_win_get_cursor(0))
    return col ~= 0 and vim.api.nvim_buf_get_text(0, line - 1, 0, line - 1, col, {})[1]:match("^%s*$") == nil
end

local cmp = require 'cmp'
local lspkind = require('lspkind')
vim.opt.completeopt = "menuone,noselect"
cmp.setup({
    formatting = {
        format = lspkind.cmp_format({
            mode = 'symbol_text',
            maxwidth = 30,
            maxheight = 10,
            symbol_map = { Copilot = "" }
        })
    },
    snippet = {
        expand = function(args)
            require('luasnip').lsp_expand(args.body) -- For `luasnip` users.
        end,
    },
    window = {
        completion = {
            border = border "CmpBorder"
        },
        documentation = {
            border = border "CmpDocBorder"
        },
    },
    mapping = cmp.mapping.preset.insert({
        ['<C-b>'] = cmp.mapping.scroll_docs(-4),
        ['<C-f>'] = cmp.mapping.scroll_docs(4),
        ['<C-Space>'] = cmp.mapping.complete(),
        ['<C-e>'] = cmp.mapping.abort(),
        ['<CR>'] = cmp.mapping.confirm({ select = true }),
        ["<Tab>"] = vim.schedule_wrap(function(fallback)
            if cmp.visible() and has_words_before() then
                cmp.select_next_item({ behavior = cmp.SelectBehavior.Select })
            else
                fallback()
            end
        end),
        ["<S-Tab>"] = cmp.mapping(function()
            if cmp.visible() then
                cmp.select_prev_item()
            end
        end, { "i", "s" }),
    }),

    sources = cmp.config.sources({
        { name = 'nvim_lsp' },
        -- { name = 'vsnip' }, -- For vsnip users.
        { name = 'luasnip' }, -- For luasnip users.
        -- { name = 'ultisnips' }, -- For ultisnips users.
        -- { name = 'snippy' }, -- For snippy users.
    }, {
        { name = 'buffer' },
    })
})

local capabilities = require('cmp_nvim_lsp').default_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true
local root_files_clangd = {
    '.clangd', '.clang-tidy', 'CMakeLists.txt', '.clang-format', 'compile_commands.json', 'compile_flags.txt',
    'configure.ac', '.git', -- AutoTools
}

vim.api.nvim_create_autocmd("FileType", {
    pattern = { "sql", "mysql", "plsql" },
    callback = function()
        local cmp = require("cmp")
        cmp.setup.buffer({
            sources = {
                { name = "vim-dadbod-completion" },
            },
        })
    end,
})

-- Root files --> CMakefile
local root_files_cmake = {
    'CMakePresets.json', 'CTestConfig.cmake', '.git', 'build', 'cmake'
}

-- Root files --> Lua
local root_files_lua = {
    ".luarc.json", ".luacheckrc", ".stylua.toml", "stylua.toml", "selene.toml", ".git", "init.lua"
}

-- Root files --> PyRight(python)
local root_files_pyright = { ".git", ".pyright", "main.py", ".pylance" }

-- Root files --> Tsserver
local root_files_tsserver = { ".git", "package-lock.json", "package.json" }

require 'lspconfig'.cmake.setup {
    capabilities = capabilities,
    on_attach = on_attach,
    cmd = { "cmake-language-server" },
    filetypes = { "cmake" },
    single_file_support = true,
    root_dir = function(fname)
        return require 'lspconfig'.util.root_pattern(unpack(root_files_cmake))(fname) or
            require 'lspconfig'.util.find_git_ancestor(fname)
    end
}

-- For Kotlin
require 'lspconfig'.kotlin_language_server.setup {
    capabilities = capabilities,
    on_attach = on_attach,
    cmd = { "kotlin-language-server" },
    filetypes = { "kotlin" },
    single_file_support = true,
    root_dir = function(fname)
        return require 'lspconfig'.util.root_pattern("settings.gradle")(fname) or
            require 'lspconfig'.util.find_git_ancestor(fname)
    end
}

-- -- PyRight Support
require 'lspconfig'.pyright.setup {
    on_attach = on_attach,
    capabilities = capabilities,
    cmd = { "pyright-langserver", "--stdio" },
    filetypes = { "python" },
    settings = {
        python = {
            analysis = {
                autoSearchPaths = true,
                diagnosticMode = "workspace",
                useLibraryCodeForTypes = true
            }
        }
    },
    single_file_support = false,
    root_dir = function(fname)
        return require 'lspconfig'.util.root_pattern(unpack(root_files_pyright))(fname) or
            require 'lspconfig'.util.find_git_ancestor(fname)
    end
}

-- Tsserver Setup
require 'lspconfig'.ts_ls.setup {
    -- on_attach = on_attach,
    -- capabilities = capabilities,
    cmd = { "typescript-language-server", "--stdio" },
    filetypes = { "javascript", "javascriptreact", "javascript.jsx", "typescript", "typescriptreact", "typescript.tsx" },
    -- single_file_support = true,
    -- init_options = {
    --     hostInfo = "neovim"
    -- },
    root_dir = function(fname)
        return --require 'lspconfig'.util.root_pattern(unpack(root_files_tsserver))(fname) or
            require 'lspconfig'.util.find_git_ancestor(fname)
    end
}

-- Docker Setup
require 'lspconfig'.dockerls.setup {
    on_attach = on_attach,
    capabilities = capabilities,
    cmd = { "docker-langserver", "--stdio" },
    filetypes = { "dockerfile" },
    single_file_support = true
}

-- Clangd Setup
require 'lspconfig'.clangd.setup {
    on_attach = on_attach,
    capabilities = capabilities,
    cmd = { "clangd" },
    filetypes = { "c", "cpp", "objc", "objcpp", "cuda" },
    single_file_support = true,
    root_dir = function(fname)
        return require 'lspconfig'.util.root_pattern(unpack(root_files_clangd))(fname) or
            require 'lspconfig'.util.find_git_ancestor(fname)
    end
}

-- For RLS (RUST)
require 'lspconfig'.rust_analyzer.setup {
    cmd = { "rust-analyzer" },
    filetypes = { "rust" },
    settings = {
        ["rust-analyzer"] = {}
    },
    root_dir = function(fname)
        return require 'lspconfig'.util.root_pattern({ ".git", ".meson" })(fname) or
            require 'lspconfig'.util.find_git_ancestor(fname)
    end
}

-- For lua sumneko
require 'lspconfig'.lua_ls.setup {
    on_attach = on_attach,
    capabilities = capabilities,
    settings = {
        Lua = {
            runtime = {
                -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
                version = 'LuaJIT',
            },
            diagnostics = {
                -- Get the language server to recognize the `vim` global
                globals = { 'vim' },
            },
            workspace = {
                -- Make the server aware of Neovim runtime files
                library = vim.api.nvim_get_runtime_file("", true),
            },
            filetypes = { "lua" },
            single_file_support = true,
            -- Do not send telemetry data containing a randomized but unique identifier
            telemetry = {
                enable = false,
            },
        },
    },
    single_file_support = true,
    filetypes = { "lua" },
    root_dir = function(fname)
        return require 'lspconfig'.util.root_pattern(unpack(root_files_lua))(fname) or
            require 'lspconfig'.util.find_git_ancestor(fname)
    end
}

-- For HTML
require 'lspconfig'.html.setup {
    on_attach = on_attach,
    capabilities = capabilities,
    cmd = { "vscode-html-language-server", "--stdio" },
    init_options = {
        configurationSection = { "html", "css", "javascript" },
        embeddedLanguages = {
            css = true,
            javascript = true
        },
        provideFormatter = true
    },
    filetypes = { "html" },
    single_file_support = false,
}

-- For CSS
require 'lspconfig'.cssls.setup {
    on_attach = on_attach,
    capabilities = capabilities,
}

-- For tailwindCSS
require 'lspconfig'.tailwindcss.setup {
    on_attach = on_attach,
    capabilities = capabilities,
    cmd = { "tailwindcss-language-server", "--stdio" },
    filetypes = { "aspnetcorerazor", "astro", "astro-markdown", "blade", "django-html", "htmldjango", "edge", "eelixir",
        "elixir", "ejs", "erb", "eruby", "gohtml", "haml", "handlebars", "hbs", "html", "html-eex", "heex", "jade",
        "leaf", "liquid", "markdown", "mdx", "mustache", "njk", "nunjucks", "php", "razor", "slim", "twig", "css", "less",
        "postcss", "sass", "scss", "stylus", "sugarss", "javascript", "javascriptreact", "reason", "rescript",
        "typescript", "typescriptreact", "vue", "svelte" },
    init_options = {
        userLanguages = {
            eelixir = "html-eex",
            eruby = "erb"
        },
    },
    settings = {
        tailwindCSS = {
            classAttributes = { "class", "className", "classList", "ngClass" },
            lint = {
                cssConflict = "warning",
                invalidApply = "error",
                invalidConfigPath = "error",
                invalidScreen = "error",
                invalidTailwindDirective = "error",
                invalidVariant = "error",
                recommendedVariantOrder = "warning"
            },
            validate = true
        }
    },
    root_dir = function(fname)
        return require 'lspconfig'.util.root_pattern('tailwind.config.js', 'tailwind.config.ts', 'postcss.config.js',
            'postcss.config.ts', 'package.json', 'node_modules', '.git')(fname) or vim.fn.getcwd()
    end
}
