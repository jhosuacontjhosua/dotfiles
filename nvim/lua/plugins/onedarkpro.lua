local onedarkpro = require("onedarkpro")
local colors = {
  bg = "#282c34",  -- Background
  fg = "#abb2bf",  -- Foreground
  ui = "#5677fc",  -- UI elements
  comment = "#707070",  -- Comments
  constant = "#c678dd",  -- Constants
  error = "#e06c75",  -- Errors
  lineNr = "#9899b7",  -- Line numbers
  special = "#d3b774",  -- Special characters
  string = "#98fb98",  -- Strings
  variable = "#6182b6",  -- Variables
  blue = "#6182b6",
  cyan = "#abb2bf",
  green = "#98fb98",  -- Can be adjusted for NeoTree highlighting
  orange = "#c678dd",
  purple = "#c678dd",  -- Can be adjusted for NeoTree highlighting
  red = "#e06c75",
  yellow = "#d3b774",  -- Can be adjusted for NeoTree highlighting
}

onedarkpro.setup({
    colors = {
    }, -- Override default colors
    highlights = {
         -- NeoTree
        NeoTreeFileIcon = { fg = colors.blue },
        NeoTreeFileNameOpened = {
            fg = colors.blue,
            style = "italic",
        },
        NeoTreeDirectoryIcon = { fg = colors.blue },
        NeoTreeRootName = { fg = colors.cyan, style = "bold" },
        NeoTreeTitleBar = { fg = "${bg}", bg = colors.blue },
        NeoTreeFloatTitle = { fg = "${bg}", bg = colors.blue },
        -- NeoTreeDirectoryIcon = { fg = colors.red, bg = colors.red },
        -- NeoTreeFadeText2 = { fg= colors.blue },
        -- EndOfBuffer = { fg= colors.blue }
    },
  options = {
    cursorline = true, -- Use cursorline highlighting?
    transparency = true, -- Use a transparent background?
    terminal_colors = true, -- Use the theme's colors for Neovim's :terminal?
    lualine_transparency = true, -- Center bar transparency?
    highlight_inactive_windows = false, -- When the window is out of focus, change the normal background?
  }

})

vim.cmd("colorscheme onedark_dark")
