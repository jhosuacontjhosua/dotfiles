vim.g.mapleader = ' '

vim.opt.number = true
vim.opt.relativenumber = true

vim.opt.cmdheight = 1
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true
vim.o.laststatus = 3
vim.o.updatetime = 50

vim.opt.smartindent = true
vim.opt.termguicolors = true
vim.opt.clipboard = "unnamedplus"
vim.opt.scrolloff = 8
vim.wo.wrap = false

vim.diagnostic.config({
    virtual_text = {
        --prefix = '■', -- Could be '●','■', '▎', 'x'
        prefix = ' ',
    },
    signs = true,
    underline = false,
    update_in_insert = true,
    severity_sort = false,
})

-- config for treesitter
require'nvim-treesitter.configs'.setup {
  highlight = {
    enable = false,
    disable = { "c", "rust" },
    disable = function(lang, buf)
        local max_filesize = 100 * 1024 -- 100 KB
        local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
        if ok and stats and stats.size > max_filesize then
            return true
        end
    end,
    additional_vim_regex_highlighting = false,
  },
}

