vim.cmd("TSEnable highlight")
local function list_files_in_directory(directory)
    local success, result = pcall(vim.fn.readdir, directory)
    if success then
        if type(result) == "table" then
            return result
        else
            vim.api.nvim_command("echo 'Error: readdir did not return a table'")
        end
    else
        vim.api.nvim_command("echo 'Error: " .. result .. "'")
    end
end

local function arrays_have_common_element(arr1, arr2)
    local set = {}
    for _, value in pairs(arr1) do
        set[value] = true
    end
    for _, value in pairs(arr2) do
        if set[value] then
            return true
        end
    end
    return false
end

-- ONLY TO USE THE OTHER PLUGIN
local project_name = vim.fn.fnamemodify(vim.fn.getcwd(), ':p:h:t')
local workspace_dir = '/tmp/jdtls/' .. project_name
local opts = { noremap = true, silent = true }
local path_base = os.getenv('HOME') .. '/dotfiles/'

local filesNow = list_files_in_directory(vim.fn.getcwd())
local resultBool = arrays_have_common_element(filesNow, { ".git", ".mvnw", "build.gradle" })

if resultBool then
    vim.keymap.set('n', '<space>e', vim.diagnostic.open_float, opts)
    vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, opts)
    vim.keymap.set('n', ']d', vim.diagnostic.goto_next, opts)
    vim.keymap.set('n', '<space>q', vim.diagnostic.setloclist, opts)

    local on_attach = function(client, bufnr)
        -- Enable completion triggered by <c-x><c-o>
        vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')
        -- See `:help vim.lsp.*` for documentation on any of the below functions
        local bufopts = { noremap = true, silent = true, buffer = bufnr }
        vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, bufopts)
        vim.keymap.set('n', 'gd', vim.lsp.buf.definition, bufopts)
        vim.keymap.set('n', 'K', vim.lsp.buf.hover, bufopts)
        vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, bufopts)
        vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, bufopts)
        vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, bufopts)
        vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, bufopts)
        vim.keymap.set('n', '<space>wl', function()
            print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
        end, bufopts)
        vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, bufopts)
        vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, bufopts)
        vim.keymap.set('n', '<space>ca', vim.lsp.buf.code_action, bufopts)
        vim.keymap.set('n', '<space>gR', vim.lsp.buf.references, bufopts)
        vim.keymap.set('n', '<space>f', function() vim.lsp.buf.format { async = true } end, bufopts)
    end

    local config = {
        on_attach = on_attach,
        flags = {
            allow_incremental_sync = true,
        },
        cmd = {
            'java',
            '-Declipse.application=org.eclipse.jdt.ls.core.id1',
            '-Dosgi.bundles.defaultStartLevel=4',
            '-Declipse.product=org.eclipse.jdt.ls.core.product',
            '-Dlog.protocol=true',
            '-Dlog.level=ALL',
            '-javaagent:' .. path_base .. 'lombok.jar',
            '-Xms1g',
            '--add-modules=ALL-SYSTEM',
            '--add-opens', 'java.base/java.util=ALL-UNNAMED',
            '--add-opens', 'java.base/java.lang=ALL-UNNAMED',
            '-jar', path_base .. 'jdtls/plugins/org.eclipse.equinox.launcher_1.6.900.v20240613-2009.jar',
            '-configuration', path_base .. 'jdtls/config_linux',
            '-data', workspace_dir,
        },
        root_dir = require('jdtls.setup').find_root({ '.git', 'mvnw', 'gradlew', 'settings.gradle' }),
        settings = {
            java = {
                eclipselsp = {
                    workspaces = {
                        {
                            name = 'eclipse',
                            root = require('jdtls.setup').find_root({ '.git', 'mvnw', 'gradlew', 'settings.gradle' }),
                        },
                    },
                },
            }
        },
    }

    require('jdtls').start_or_attach(config)
else
    vim.api.nvim_command(":lua print('Single file mode..true.')")
end
