"Created for compatibility with LINUX and Windows

set number relativenumber
set tabstop=4
set shiftwidth=4
set shiftwidth=4
set expandtab
set softtabstop=4
set nowrap
set paste
syntax on

call plug#begin()

"Plug 'arcticicestudio/nord-vim'
Plug '0xstepit/flow.nvim'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'preservim/nerdtree'
Plug 'chriskempson/vim-tomorrow-theme'
Plug 'github/copilot.vim'

call plug#end()

colorscheme Tomorrow-Night-Bright

map <C-n> :NERDTreeToggle<CR>
"map <C-n> :Files<CR>
nmap <C-b> :Buffers<cr>
nmap <C-r> :Rg<cr>
nmap <C-t> :GFiles<cr>
